extends KinematicBody
var speed = 55
var speedrate = 10
var jump_speed = 9
var accel = 0
var velocity = Vector3(0,0,0)
var rot_y = Vector3(0,0,0)
var rot_y2 = rot_y

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	set_as_toplevel(true)
	translate(Vector3(0,10,0))
	rot_y = $"/root/Globals".rot_y
	set_rotation_degrees(Vector3(0,0,0))
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	velocity += transform.basis.z * accel * delta
	accel+=speedrate
	if velocity.z < -1:
		velocity.z = -1
	velocity = move_and_slide(velocity,Vector3(0,0,0))
	pass

func _on_buster_shot_area_area_entered(area):
	queue_free()
	pass # Replace with function body.
