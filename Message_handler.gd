extends Node2D
var default_script = load("res://gui/npc_dialogue/message_box.tscn")
var script_c = default_script
var script_q = load("res://gui/question_gui.tscn")
var current_scene
var inst = script_c.instance()
var inst2 = script_q.instance()
var intro = 0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if $"/root/Globals".area == "Demo Ruin B1" and $"/root/Globals".demo_scn_flag == 0:
		msg()
#	current_scene = $"/root/Globals".current_scene
	current_scene = get_tree().get_current_scene()
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if $"/root/Globals".question == 1:
		$"/root/Globals".question = 2
		qst()
	
	if $"/root/Globals".question == 3:
		$"/root/Globals".question = 4
		msg()
	
	if $"/root/Globals".area == "Demo Ruin B1" and $"/root/Globals".demo_scn_flag == 0 and intro == 0:
		msg()
		intro = 1
	current_scene = get_tree().get_current_scene()
	if Input.is_action_just_pressed("test_message") and $"/root/Globals".near_npc == 1 and $"/root/Globals".wait == 0:
		msg()
		pass

	if Input.is_action_just_pressed("test_message") and $"/root/Globals".locked == 1 and $"/root/Globals".wait == 0:
		msg()
		pass

	if $"/root/Globals".soda == 1 and $"/root/Globals".wait == 1:
		msg()
		$"/root/Globals".wait = 2
		pass

func msg(): #Message Spawner
	inst = script_c.instance()
	print("Dialogue Loaded")
	add_child(inst,true)
	pass

func qst(): #Question Spawner
	inst2 = script_q.instance()
	print("Question Loaded")
	add_child(inst2,true)
	pass
