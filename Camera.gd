extends Camera
var cam_direction

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$"/root/Globals".cam_direction = global_transform.origin
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$"/root/Globals".cam_direction = global_transform.origin
	pass
