Exported from https://mml.dashgl.com/mml.dashgl.com/tools/mml2_pc_mesh/
Attribution appreciated not requied (Kion at Dashgl.com)
Source Code is avaiable under GPLv3 License https://gitlab.com/kion-dgl/JS_MML2_PC_MESH

Included Formats:
.dae Collada format (usable with Blender)
.png Texture for Collada file
.json Threejs JSON format https://github.com/mrdoob/three.js/
.gltf GL Transmission Format https://github.com/KhronosGroup/glTF
.glb Binary version of gltf
.json, .gltf, and .glb can be viewed at: https://threejs.org/editor/