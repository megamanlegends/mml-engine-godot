extends KinematicBody

export var speed = 25
var DOWN = -1
var accel = 0.00
export var speedrate = 50
var direction = Vector3()
var gravity = -20
var velocity = Vector3()
var state = "stand"
var rot_x = 0
var rot_y = 0
var rot_z = 0
var character = 0
var move_rot_y = 1
var facing = 180
var print_timer = 0
export var left = 0 #Starting directions.
export var right = 0
export var up = 0
export var down = 0
var jump = 0
var timer = 0
export var wait = 0
export var AI_Mode = 0 #AI Mode: 0 = Roam, 1 = Patrol up/down 2 = Patrol left/right, 3 = Scan, 4 = Alert 5 = Chase.
export var Type = 0 #AI Colour
export var spin = 0.125
var AI_Wait = 0
var hp
var maxhp = 32
var hit_timer 
var combo = 0
var combo_timer = 0
var previous_state
var name2 = get_name()
var destroyed = preload("res://Models/small_explosion.tscn")
var shoot = 0
export var enemy_required = 0
var gravity_default = Vector3(0,DOWN,0)*12
var gravity_stop = Vector3(0,DOWN,0)*0



func _ready():
	randomize()
	state = "stand"
	# Called when the node is added to the scene for the first time.
	# Initialization here
	hp = maxhp
	$Reaverbot_Root/AnimationPlayer.play("anim_000")
	print(facing)
	pass




func _process(delta):
		
		#AI Controls 2
	if Input.is_key_pressed(KEY_T):
		wait = 1
		
	if Input.is_key_pressed(KEY_Y):
		state = "stand"
		up = 0
		down = 0
		left = 0
		right = 0
		wait = 0
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.

func _physics_process(delta):
	
	if AI_Mode == 5:
		up = 1
	
#	print("State =",state)
#	print("On Floor = ",is_on_floor())
#	print("Touching Wall = ",is_on_wall())
	
	if combo_timer > 0:
		combo_timer -=1*delta
		if combo_timer < 0.02:
			combo = 0.00
			combo_timer = 0.00
	
	
	
	if not $zako_sfx.current_animation == $Reaverbot_Root/AnimationPlayer.current_animation:
		$zako_sfx.play($Reaverbot_Root/AnimationPlayer.current_animation)
		pass
	#AI Controls 1
#	print(AI_Wait)
	if AI_Wait > 0:
		AI_Wait -=5*delta
		if AI_Wait < 1:
			AI_Wait = 0
			
	
	timer +=1
	if timer > 120 and AI_Wait < 1 and not state == "knockback" and not state == "hurt" and AI_Mode == 0:
		down = randi() % 2
		up = randi() % 2
		left = randi() % 2
		right = randi() % 2
		timer = 0
		pass
	
	if AI_Mode == 4:
		
		pass
#	print(state)
#	print(hp)
	if state == "hurt":
		hit_timer -=1
		wait = 1
		$Reaverbot_Root/AnimationPlayer.play("anim_005")
		if hit_timer <1:
			if hp < 1:
				explode()
			state = previous_state
			wait = 0
			hit_timer = 0
		pass
	
	if $Reaverbot_Root/Wall_Detection.is_colliding() and AI_Wait < 1 and not state == "knockback" and not state == "hurt":
		print(get_name()+" has bumped into a wall")
		if up == 1:
			down = 1
			left = 0
			right = 0
			up = 0
			AI_Wait = 5
			return
		
		if down == 1:
			up = 1
			down = 0
			left = 0
			right = 0
			AI_Wait = 5
			return
			
		if left == 1:
			right = 1
			up = 0
			down = 0
			left = 0
			AI_Wait = 5
			return
			
		if right == 1:
			left = 1
			up = 0
			down = 0
			right = 0
			AI_Wait = 5
			return
	
	if wait == 1 and not state == "hurt" and not state == "knockback":
		state = "stand"
		left = 0
		right = 0
		up = 0
		down = 0
	move_rot_y +=0.01
	if left == 1:
		right = 0
		up = 0
		down = 0
#		print("left")

	if right == 1:
		left = 0
		up = 0
		down = 0
#		print("right")


	if up == 1:
		left = 0
		right = 0
		down = 0
#		print("up")


	if down == 1:
		left = 0
		right = 0
		up = 0
#		print("down")

	if accel > speed:
		accel = speed


	#Controls and Animations
	direction = Vector3(0,0,0)

	if left == 1 and up == 0 and down == 0 and right == 0 and not state == "hurt" and not state == "knockback":
		up = 0
		down = 0
		direction.x -=1+accel
		accel +=speedrate
		if state != "jump":
			state = "walk"
		if rot_y > 359 and not down == 1 and not down == 1:
			rot_y = 0

		if rot_y < 270 and rot_y > 0 and not up == 1 and not down == 1:
			rot_y+=10

		if rot_y < 1 and not up == 1 and not down == 1:
			rot_y-=20
			if rot_y < -89:
				rot_y = 270
		$Reaverbot_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_001" and state == "walk":
			$Reaverbot_Root/AnimationPlayer.play("anim_001")

	if right == 1 and up == 0 and down == 0 and left == 0 and not state == "hurt" and not state == "knockback":
		up = 0
		down = 0
		direction.x +=1+accel
		accel +=speedrate
		if state != "jump":
			state = "walk"

		if rot_y > 359 and not up == 1 and not down == 1:
			rot_y = 0

		if rot_y > 90 and not up == 1 and not down == 1:
			rot_y -=10

		if rot_y < 90 and not up == 1 and not down == 1:
			rot_y+=10

		$Reaverbot_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_001" and state == "walk":
			$Reaverbot_Root/AnimationPlayer.play("anim_001")

	if up == 1 and left == 0 and right == 0 and down == 0 and not state == "hurt" and not state == "knockback":
		left = 0
		right = 0
		if state != "jump":
			state = "walk"
		if rot_y > 180:
			rot_y -=10
		if rot_y < 180:
			rot_y +=10
		$Reaverbot_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_001" and state == "walk":
			$Reaverbot_Root/AnimationPlayer.play("anim_001")

	if down == 1 and left == 0 and right == 0 and up == 0 and not state == "hurt" and not state == "knockback":
		right = 0
		left = 0
		direction.z +=1+accel
		accel +=speedrate*delta
		if state != "jump":
			state = "walk"
			if rot_y > 269:
				rot_y +=10
			if rot_y < 181 and rot_y > 0:
				rot_y -=10
			if rot_y > 359:
				rot_y = 0
		$Reaverbot_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_001" and state == "walk":
			$Reaverbot_Root/AnimationPlayer.play("anim_001")
#
#	if Input.is_action_pressed("Turn_Left"):
#		rotate_y(spin/5)
#		pass
##
#	if Input.is_action_pressed("Turn_Right"):
#		rotate_y(-spin/5)
#		pass


	if $Reaverbot_Root/Floor_Check.is_colliding() == true:
		gravity = gravity_stop
	if $Reaverbot_Root/Floor_Check.is_colliding() == false:
		gravity = gravity_default



	#Movement and gravity.
	direction = direction.normalized()
	direction = direction * accel * delta

	if velocity.y > 2 and $Reaverbot_Root/Floor_Check.is_colliding() == false:
		gravity = -30
	else:
		gravity = -20

	velocity.y += gravity * delta
	velocity.x = direction.x
	velocity.z = direction.z

	if state == "stand" and not state == "hurt" and not state == "knockback":
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_000":
			$Reaverbot_Root/AnimationPlayer.play("anim_000")
		up = 0
		down = 0
		left = 0
		right = 0



	#Collision detection
	if is_on_floor() and jump == 1:
		state = "jump"
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_005":
			$Reaverbot_Root/AnimationPlayer.play("anim_005")
		velocity.y = 9

	if velocity.y > 4 or velocity.y < -1 and $Reaverbot_Root/AnimationPlayer.current_animation == "anim_005":
			$Reaverbot_Root/AnimationPlayer.play("anim_006")
			state = "jump"
			pass

	if not is_on_floor() and velocity.y < -2:
		state = "jump"
		if state == "jump":
			velocity.y += gravity * delta
			pass

	if state == "jump":
		if not $Reaverbot_Root/AnimationPlayer.current_animation == "anim_006" and velocity.y >= 4 or velocity.y <= -4:
			$Reaverbot_Root/AnimationPlayer.play("anim_006")

	if is_on_floor():
		if state != "stand" and velocity.x == 0 and velocity.z == 0 and not state == "hurt" and not state == "knockback":
			state = "stand"

		if state == "jump":
			state = "stand"
	
	if state == "knockback":
		wait = 0
		if left == 0 and right == 0 and up == 0 and down == 0:
			down = randi() % 2
			up = randi() % 2
			left = randi() % 2
			right = randi() % 2
			
			direction.z -=1+accel
			accel +=speedrate*2
			velocity.y = 9
			velocity.z = direction.z
		
		if left == 1:
			direction.x -=1+accel
			accel +=speedrate
			velocity.y = 9
			velocity.x = direction.x
			pass
		
		if right == 1:
			direction.x +=1+accel
			accel +=speedrate
			velocity.y = 9
			velocity.x = direction.x
			pass
		
	if up == 1 and not left == 1 and not right == 1:
		velocity += -transform.basis.z *accel *delta
		accel+=speedrate*delta
		if not state == "jump":
			state = "run"
			if rot_y > 180:
				rot_y-=10
			if rot_y < 180:
				rot_y +=10
			$Reaverbot_Root.set_rotation_degrees(Vector3(0,rot_y,0))
		
		if down == 1:
			direction.z -=1+accel
			accel +=speedrate*2*delta
			velocity.y = 9
			velocity.z = direction.z
			pass
	velocity = move_and_slide(velocity,Vector3(0,1,0))

func _on_AnimationPlayer_animation_finished(anim_005):
	if not state == "hurt" and not state == "knockback":
		state = "stand"
	pass # replace with function body


func _on_AnimationPlayer_animation_started(anim_001):
	pass


func _on_Area_area_entered(area): #Player Bullet Mask Detection
	pass # Replace with function body.


func _on_Bullet_Check_PL_area_entered(area):
	hit_timer = 20
	combo_timer = 1
	combo+=1
	if not state == "hurt" and not state == "knockback":
		previous_state = state
	
	hp-=4*$"/root/Globals".power
	$reaver_sfx/hit_sfx.play()
	if combo < 5 and not state == "knockback":
		if hp < 1:
			explode()
		state = "hurt"
	if combo > 5:
		$reaver_sfx/knockdown_sfx.play()
		if hp > 0 :
			state = "knockback"
		if hp < 1:
			state = "hurt"
	pass # Replace with function body.

func explode():
	$"/root/Globals".defeated_enemies +=1
	print("Reaverbot Location = ",global_transform)
	var b = destroyed.instance()
	b.start($explosion.global_transform)
	get_tree().get_root().add_child(b)
	queue_free()
