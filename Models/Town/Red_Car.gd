extends KinematicBody
export var speed = 0.00
export var joke = 0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	if speed < 1:
		$Area.set_collision_layer_bit(10,false)
		$Area.set_collision_mask_bit(10,false)
	if joke == 1:
		$Area.set_collision_layer_bit(10,true)
		$Area.set_collision_mask_bit(10,true)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if joke == 1:
		$Area.set_collision_layer_bit(10,true)
		$Area.set_collision_mask_bit(10,true)
		$"/root/Globals".joke = 1
		
	if speed < 1:
		$Area.set_collision_layer_bit(10,false)
		$Area.set_collision_mask_bit(10,false)
	pass
