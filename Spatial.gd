extends Area
var b
var speed = 140
var velocity = Vector3()
var energy
var power
var ranged
var rapid
var life
var shot_sfx = preload("res://shot_miss_sfx.tscn")
var shot_sfx_inst = shot_sfx.instance()
func _ready():
	#Called when the scene starts.
	$shot_sfx.play()
	energy = $"/root/Globals".energy
	ranged = $"/root/Globals".ranged
	rapid = $"/root/Globals".rapid
	power = $"/root/Globals".power
	$"/root/Globals".energy -=1
	life = ranged

	
	pass

func start(xform):
	transform = xform
	velocity = -transform.basis.z * speed

func _process(delta):
	transform.origin += velocity * delta
	life-=1*delta
	if $bullet_miss_check.is_colliding():
		b = shot_sfx.instance()
		get_parent().add_child(b)
		print("Buster Shot has hit the wall")
		$"/root/Globals".energy+=1
		queue_free()
	if life < 0.01:
		$"/root/Globals".energy+=1
		queue_free()

func _on_Timer_timeout():
	queue_free()

func _on_Bullet_body_entered(body):
	$"/root/Globals".energy+=1
	queue_free()
	pass

func _on_Buster_Shot_body_entered(body):
	b = shot_sfx.instance()
	get_parent().add_child(b)
	$"/root/Globals".energy+=1
	queue_free()
	pass # Replace with function body.


func _on_Buster_Shot_area_entered(area):
	$"/root/Globals".energy+=1
	queue_free()
	pass # Replace with function body.
